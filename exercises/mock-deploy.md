# (Mock) deploy

Deployment is delivering the binary artifacts and infra-structure desired end-state to an environment.

## Goals

Delivering the binaries and infrastructure desired end-state to an environment. However in this setup an actual 
delivery is not an option. This will be a mock deployment

## Approach

N/A

## Exercise

In this exercise a mock deployment step is created

### Step 1: The mock deployment

Create a mock_deploy job and deploy and add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test
  - uat
  - deploy


# The mock deployment job
mock_deploy:
  stage: deploy
  script:
  - echo "This is a mock deploy"
```

The stage **deploy** is an new logical divider of steps within the continuous delivery pipeline. The job **mock_deploy** is 
actual command that starts the building (bundling) of the application. 
